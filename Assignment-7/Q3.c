# include <stdio.h>
#define SIZE 100
int a[SIZE][SIZE], b[SIZE][SIZE], result[SIZE][SIZE];
int arows, brows, acolumns, bcolumns;
int i, j, k, user;
int total = 0;

int add_matrices();
int multiply_matrices();

int main() {
    printf("(1) Add Matrices\n(2) Multiply Matrices\n\n");
    scanf("%d", &user);
    if (user == 1) {
        add_matrices();
    } else if (user == 2) {
        multiply_matrices();
    } else {
        printf("Invalid input!");
    }
    return 0;
}

int add_matrices() {
    printf("\nEnter the number of rows and columns of the Matrix A :");
    scanf("%d %d", &arows, &acolumns);
    puts("\nEnter the elements of Matrix A:\n");
    for (i = 0; i < arows; i++) {
        for (j = 0; j < acolumns; j++) {
            scanf("%d", &a[i][j]);
        }
    }
    
    printf("\nEnter the number of rows and columns of the Matrix B :");
    scanf("%d %d", &brows, &bcolumns);
    if ((acolumns != bcolumns) || (arows != brows)) {
        printf("\nMatrix addition is not possible\n");
        goto end;
    }
    puts("\nEnter the elements of Matrix B:\n");
    for (i = 0; i < brows; i++) {
        for (j = 0; j < bcolumns; j++) {
            scanf("%d", &b[i][j]);
        }
    }
   
    for (i = 0; i < arows; i++) {
        for (j = 0; j < acolumns; j++) {
            result[i][j] = a[i][j] + b[i][j];
        }
    }
    printf("\nResult:\n\n");
    for (i = 0; i < arows; i++) {
        for (j = 0; j < acolumns; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }
    end:
    return 0;
}

int multiply_matrices() {
   
    printf("\nEnter the number of rows and columns of the Matrix A :");
    scanf("%d %d", &arows, &acolumns);
    puts("\nEnter the elements of Matrix A:\n");
    for (i = 0; i < arows; i++) {
        for (j = 0; j < acolumns; j++) {
            scanf("%d", &a[i][j]);
        }
    }
   
    printf("\nEnter the number of rows and columns of the Matrix B:");
    scanf("%d %d", &brows, &bcolumns);
   
    if (acolumns != brows) {
        printf("\nMatrix product is not possible\n");
        goto end;
    }
    puts("\nEnter the elements of Matrix B:\n");
    for (i = 0; i < brows; i++) {
        for (j = 0; j < bcolumns; j++) {
            scanf("%d", &b[i][j]);
        }
    }
    for (i = 0; i < arows; i++) {
        for (j = 0; j < bcolumns; j++) {
            for (k = 0; k < brows; k++) {
                total += a[i][k] * b[k][j];
            }
            result[i][j] = total;
            total = 0;
        }
    }
    printf("\nResult:\n\n");
    for (i = 0; i < arows; i++) {
        for (j = 0; j < bcolumns; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }
    end:
    return 0;
}